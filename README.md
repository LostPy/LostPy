# Welcome !

J'ai 24 ans, je m'appelle Thomas et je suis autodidacte en programmation depuis plusieurs années maintenant j'utilise principalement le langage ✨ Python ✨ et ✨ Rust ✨.

Je réalise essentiellement des packages Python / Rust, des programmes sur microcontrolleur (en Rust sur stm32 et Arduino), des simulations ainsi que de temps en temps des applications Qt (en Python).

J'ai également un petit site où je présente mes projets et quelque tutoriel : https://lostpy.gitlab.io/.

## Environnement de programmation

 * Système d'exploitation : Debian/Ubuntu
 * Editeurs de code : [Helix](https://github.com/helix-editor/helix), [Zed](https://zed.dev/)
 * Ancien éditeur de code utilisés :
    - [Lapce](https://lapce.dev/)
    - Vim
    - Sublime Text
    - PyCharm
    - Spyder
 * Versioning : Git

## Principaux projets

 * Une simulation de colonie de fourmie [en Python](https://gitlab.com/lostpy/pygaco) et [en Rust](https://gitlab.com/lostpy/rust-ant-simulator).
 * Un package Python pour Qt5 (PyQt5 et PySide2) et Qt6 (PySide6) regroupant [mes widgets utiles](https://gitlab.com/LostPy/qtcustomwidgets).
 * [Un driver](https://gitlab.com/LostPy/seven-segments-driver) [`embedded-hal`](https://crates.io/crates/embedded-hal) pour faciliter l'utilisation d'afficheur 7-segments.
 * Un Package Python pour [réaliser des simulations rapidement avec Qt](https://gitlab.com/LostPy/qpysimu). Je prévois de l'améliorer suite à mon expérience avec [arcade](https://api.arcade.academy/en/latest/get_started.html).
 * Un package Python pour [générer des "map"](https://gitlab.com/LostPy/mapgenerator), basé sur l'aléatoire en faisant de la génération procédural. Support pour [arcade](https://api.arcade.academy/en/latest/get_started.html) et [pygame](https://www.pygame.org)

 ## Quelques tutoriels

  * [Démarrer la programmation Arduino avec Rust](https://lostpy.gitlab.io/articles/articles/rust-arduino-install/)
  * [Utiliser Mkdocs & material pour réaliser un site statique](https://lostpy.gitlab.io/articles/articles/mkdocs-material/)
